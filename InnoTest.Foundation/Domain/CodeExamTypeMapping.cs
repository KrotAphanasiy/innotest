﻿using InnoTest.Foundation.Domain.Base;

namespace InnoTest.Foundation.Domain
{
    public class CodeExamTypeMapping: BaseEntity
    {
        public int CodeId { get; set; }
        public int ExamTypeId { get; set; }
        
        public virtual Code Code { get; set; }
        public virtual ExamType ExamType { get; set; }
    }
}