﻿using System.Threading;
using System.Threading.Tasks;
using InnoTest.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InnoTest.Data.Repositories
{
    public class UnitOfWork<TContext> : IUnitOfWork where TContext : DbContext
    {
        protected readonly TContext _context;

        public UnitOfWork(TContext context)
        {
            _context = context;
        }

        public Task SaveAsync(CancellationToken cancellationToken = default)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }

        public void ClearTracker()
        {
            _context.ChangeTracker.Clear();
        }
    }
}