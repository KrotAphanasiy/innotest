﻿using InnoTest.Foundation.Domain;

namespace InnoTest.Data.Repositories.Interfaces
{
    public interface ICodeRepository: IBaseRepository<Code>
    {
        
    }
}