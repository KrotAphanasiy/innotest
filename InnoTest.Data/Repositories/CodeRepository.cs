﻿using InnoTest.Data.Repositories.Interfaces;
using InnoTest.Foundation.Domain;
using Microsoft.EntityFrameworkCore;

namespace InnoTest.Data.Repositories
{
    public class CodeRepository: BaseRepository<Code>, ICodeRepository
    {
        public CodeRepository(InnoTestContext context, QueryTrackingBehavior queryTrackingBehavior = QueryTrackingBehavior.TrackAll) : base(context, queryTrackingBehavior)
        {
        }
    }
}