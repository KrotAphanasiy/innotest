﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using InnoTest.Data.Repositories.Interfaces;
using InnoTest.Foundation.Domain.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace InnoTest.Data.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// Data context for connecting to the database
        /// </summary>
        protected InnoTestContext _context;

        public BaseRepository(InnoTestContext context, QueryTrackingBehavior queryTrackingBehavior = QueryTrackingBehavior.TrackAll)
        {
            _context = context;

            _context.ChangeTracker.QueryTrackingBehavior = queryTrackingBehavior;
        }

        #region Select

        public async Task ExecuteSql(FormattableString sql, CancellationToken ct)
        {
            await _context.Database.ExecuteSqlInterpolatedAsync(sql, ct);
        }

        public Task<TEntity[]> SelectAsync(CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
        {
            return SelectByConditionAsync(x => true, cancellationToken, includes);
        }

        public Task<TEntity[]> SelectByConditionAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
        {
            return QueryableSelect(predicate, includes).ToArrayAsync(cancellationToken);
        }

        public Task<TEntity[]> SelectIncludeAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>[] includes)
        {
            return QueryableInclude(predicate, includes).ToArrayAsync(cancellationToken);
        }

        #endregion


        #region Get by Id, First, FirsOrDefault, Single, SingleOrDefault

        public Task<TEntity> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public Task<TEntity> GetByIdAsync(int id, CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
        {
            return FirstOrDefaultAsync(x => x.Id == id, cancellationToken, includes);
        }

        public Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
        {
            return QueryableSelect(predicate, includes).FirstOrDefaultAsync(cancellationToken);
        }

        public Task<TEntity> FirstOrDefaultIncludeAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>[] includes)
        {
            return QueryableInclude(predicate, includes).FirstOrDefaultAsync(cancellationToken);
        }

        public Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
        {
            return QueryableSelect(predicate, includes).SingleOrDefaultAsync(cancellationToken);
        }
        
        public IQueryable<TEntity> TrackingQueryableSelect(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            var result = _context.Set<TEntity>().AsTracking().Where(predicate);

            if (includes != null)
            {
                foreach (var include in includes)
                {
                    result = result.Include(include).AsTracking();
                }
            }

            return result;
        }

        #endregion


        #region Queryable

        public IQueryable<TEntity> QueryableSelect(params Expression<Func<TEntity, object>>[] includes)
        {
            return QueryableSelect(pred => true, includes);
        }

        public IQueryable<TEntity> TrackingQueryableSelect(params Expression<Func<TEntity, object>>[] includes)
        {
            var result = _context.Set<TEntity>().AsTracking();

            if (includes != null)
            {
                foreach (var include in includes)
                {
                    result = result.Include(include).AsTracking();
                }
            }

            return result;
        }

        public IQueryable<TEntity> QueryableSelect(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            var result = _context.Set<TEntity>().Where(predicate);

            if (includes != null)
            {
                foreach (var include in includes)
                {
                    result = result.Include(include);
                }
            }

            return result;
        }

        public IQueryable<TEntity> QueryableInclude(Expression<Func<TEntity, bool>> predicate, params Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>[] includes)
        {
            var result = _context.Set<TEntity>().Where(predicate);

            if (includes != null)
            {
                foreach (var include in includes)
                {
                    result = include(result);
                }
            }

            return result;
        }

        #endregion

        #region Aggregate function

        public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate = null, CancellationToken cancellationToken = default)
        {
            var query = _context.Set<TEntity>();

            return predicate == null
                ? query.CountAsync(cancellationToken)
                : query.CountAsync(predicate, cancellationToken);
        }

        public Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate = null, CancellationToken cancellationToken = default)
        {
            var query = _context.Set<TEntity>();

            return predicate == null
                ? query.AnyAsync(cancellationToken)
                : query.AnyAsync(predicate, cancellationToken);
        }

        #endregion

        public async Task<TEntity> ApplyAsync(TEntity entity, CancellationToken ct = default)
        {
            var isExists = await IsExistsAsync(entity.Id, ct);
            if (isExists)
            {
                
                if (_context.Entry(entity).State != EntityState.Modified)
                {
                    _context.Update(entity);
                }
            }
            else
            {
                _context.Add(entity);
            }

            return entity;
        }

        private Task<bool> IsExistsAsync(int id, CancellationToken ct)
        {
            return _context.Set<TEntity>().AnyAsync(x => x.Id == id, ct); //начать трекать только по необходимости иначе .AsNoTracking()
        }
        

        public async Task<bool> DeleteAsync(TEntity entity, CancellationToken ct = default)
        {
            if (await IsExistsAsync(entity.Id, ct))
            {
                _context.Remove(entity);
                return true;
            }
            else
            {
                throw new Exception($"Entity with id {entity.Id} not found!");
            }
        }

        public async Task<bool> DeleteAsync(int entityId, CancellationToken ct = default)
        {
            var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == entityId, ct);
            if (entity != null)
            {                
                _context.Remove(entity);
                return true;
            }
            else
            {
                throw new Exception($"Entity with id {entityId} not found!");
            }
        }

        public void Remove(TEntity entity)
        {
            _context.Remove(entity);
        }

        public async Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken ct = default)
        {
            foreach (var entity in entities)
            {
                await DeleteAsync(entity, ct);
            }
        }
        
        public void RemoveRange(IEnumerable<TEntity> entities)
        {
           _context.Set<TEntity>().RemoveRange(entities);
        }

        public void Add(TEntity entity)
        {
            _context.Add(entity);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}