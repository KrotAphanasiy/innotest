﻿using InnoTest.Data.Repositories;
using InnoTest.Data.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace InnoTest.Data.Configuration
{
    public static class DataLayerDependencyConfigurator
    {
        public static void ConfigureRepositories(this IServiceCollection services)
        {
            services.AddScoped<ICodeEduFormMappingRepository, CodeEduFormMappingRepository>();
            services.AddScoped<ICodeExamTypeMappingRepository, CodeExamTypeMappingRepository>();
            services.AddScoped<ICodeRepository, CodeRepository>();
            services.AddScoped<IEduFromRepository, EduFormRepository>();
            services.AddScoped<IExamTypeRepository, ExamTypeRepository>();
            services.AddScoped<ISubjectRepository, SubjectRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork<InnoTestContext>>();
        }
    }
}