﻿using System.Threading.Tasks;
using InnoTest.Data;
using InnoTest.Data.Repositories.Interfaces;
using InnoTest.UnitTests;
using InnoTest.UnitTests.Data;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace InnotTest.UnitTests.Repositories.Tests
{
    public class RepositoryTest: BaseDbTest
    {
        private readonly InnoTestContext _dbContext;
        
        private readonly ICodeEduFormMappingRepository _codeEduFormMappingRepository;
        private readonly ICodeExamTypeMappingRepository _examTypeMappingRepository;
        private readonly ICodeRepository _codeRepository;
        private readonly IEduFromRepository _eduFromRepository;
        private readonly IExamTypeRepository _examTypeRepository;
        private readonly ISubjectRepository _subjectRepository;
        public RepositoryTest()
        {
            _dbContext = ServiceProvider.GetRequiredService<InnoTestContext>();
            _codeRepository = ServiceProvider.GetRequiredService<ICodeRepository>();
            _subjectRepository = ServiceProvider.GetRequiredService<ISubjectRepository>();
            _eduFromRepository = ServiceProvider.GetRequiredService<IEduFromRepository>();
            _examTypeRepository = ServiceProvider.GetRequiredService<IExamTypeRepository>();
            _codeEduFormMappingRepository = ServiceProvider.GetRequiredService<ICodeEduFormMappingRepository>();
            _examTypeMappingRepository = ServiceProvider.GetRequiredService<ICodeExamTypeMappingRepository>();
        }
        
        [Fact]
        public async Task TestAllSelections()
        {
            await SeedAllData();

            Assert.True(await _codeRepository.FirstOrDefaultAsync(x => true) != null);
            Assert.True(await _subjectRepository.FirstOrDefaultAsync(x => true) != null);
            Assert.True(await _eduFromRepository.FirstOrDefaultAsync(x => true) != null);
            Assert.True(await _examTypeRepository.FirstOrDefaultAsync(x => true) != null);
            
            Assert.True(await _examTypeMappingRepository.FirstOrDefaultAsync(x => true) != null);
            Assert.True(await _codeEduFormMappingRepository.FirstOrDefaultAsync(x => true) != null);

            _dbContext.Database.EnsureDeleted();
        }

        private async Task SeedAllData()
        {
            await _dbContext.SeedSubjects();
            await _dbContext.SeedEduForms();
            await _dbContext.SeedExamTypes();
            await _dbContext.SeedCodes();
            await _dbContext.SeedCodeEduFormMappings();
            await _dbContext.SeedCodeExamTypeMappings();
        }
    }
}