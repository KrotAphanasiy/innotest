﻿using InnoTest.Foundation.Domain;

namespace InnoTest.Data.Repositories.Interfaces
{
    public interface ICodeExamTypeMappingRepository: IBaseRepository<CodeExamTypeMapping>
    {
        
    }
}