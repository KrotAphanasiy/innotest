using System.Diagnostics;
using System.Text;
using InnoTest.Configuration;
using InnoTest.Core.Configuration;
using InnoTest.Core.Services.Interfaces;
using InnoTest.Data;
using InnoTest.Data.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;

namespace InnoTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureServices();
            services.ConfigureRepositories();
            //Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            
            services.AddDbContext<InnoTestContext>((serviceProvider, options) =>
            {
                if (Debugger.IsAttached)
                {
                    options.EnableSensitiveDataLogging();
                    options.UseLoggerFactory(serviceProvider.GetRequiredService<ILoggerFactory>());
                }

                /*options.UseSqlServer(Configuration.GetSection("ConnectionStrings")
                    .GetValue<string>("DefaultConnection"));*/
                
                var provider = Configuration.GetValue("Provider", "SqlServer");
                switch (provider)
                {
                    case "SqlServer":
                        options.UseSqlServer(Configuration.GetSection("ConnectionStrings")
                                .GetValue<string>("DefaultConnection"),
                            sqlServerOptions =>
                            {
                                sqlServerOptions.CommandTimeout(600);
                            });
                        break;
                    case "Sqlite":
                        options.EnableSensitiveDataLogging();
                        options.UseSqlite("Data Source=database.db;", 
                            x => x.MigrationsAssembly("InnoTest.UnitTest.Migrations"));
                        break;
                }
            });
            
            services.AddSwaggerDocumentation();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwaggerDocumentation();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Hello World!"); });
                endpoints.MapControllers();
            });
        }
    }
}