﻿using InnoTest.Data.Repositories.Interfaces;
using InnoTest.Foundation.Domain;
using Microsoft.EntityFrameworkCore;

namespace InnoTest.Data.Repositories
{
    public class CodeEduFormMappingRepository: BaseRepository<CodeEduFormMapping>, ICodeEduFormMappingRepository
    {
        public CodeEduFormMappingRepository(InnoTestContext context, QueryTrackingBehavior queryTrackingBehavior = QueryTrackingBehavior.TrackAll) : base(context, queryTrackingBehavior)
        {
        }
    }
}