﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InnoTest.Data.Migrations
{
    public partial class MovedPriorityToSubject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubjectPriority",
                table: "Codes");

            migrationBuilder.AddColumn<int>(
                name: "Priority",
                table: "Subjects",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Priority",
                table: "Subjects");

            migrationBuilder.AddColumn<int>(
                name: "SubjectPriority",
                table: "Codes",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
