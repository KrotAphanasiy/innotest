﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using InnoTest.Core.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InnoTest.Controllers
{
    [Route("api/v1/[controller]")]
    public class ImportController: ControllerBase
    {
        private readonly IExcelImportService _excelImportService;
        public ImportController(IExcelImportService excelImportService)
        {
            _excelImportService = excelImportService;
        }

        [HttpPost]
        public async Task<IActionResult> Import(IFormFile file, CancellationToken ct = default)
        {
            if (file == null || file.Length == 0)  
                return StatusCode(400,"No file posted.");

            using (var ms = new MemoryStream())
            {
                await file.CopyToAsync(ms);
                await _excelImportService.ImportToDatabaseAsync(ms, file.FileName, ct);    
            }
            
            return Ok();
        }
    }
}