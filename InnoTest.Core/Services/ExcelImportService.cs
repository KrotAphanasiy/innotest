﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using ExcelDataReader;
using InnoTest.Core.Services.Interfaces;
using InnoTest.Data.Repositories.Interfaces;
using InnoTest.Foundation.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace InnoTest.Core.Services
{
    public class ExcelImportService: IExcelImportService
    {
        private readonly ICodeEduFormMappingRepository _codeEduFormMappingRepository;
        private readonly ICodeExamTypeMappingRepository _examTypeMappingRepository;
        private readonly ICodeRepository _codeRepository;
        private readonly IEduFromRepository _eduFromRepository;
        private readonly IExamTypeRepository _examTypeRepository;
        private readonly ISubjectRepository _subjectRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ExcelImportService(ICodeEduFormMappingRepository codeEduFormMappingRepository, ICodeExamTypeMappingRepository examTypeMappingRepository, ICodeRepository codeRepository, IEduFromRepository eduFromRepository, IExamTypeRepository examTypeRepository, ISubjectRepository subjectRepository, IUnitOfWork unitOfWork)
        {
            _codeEduFormMappingRepository = codeEduFormMappingRepository;
            _examTypeMappingRepository = examTypeMappingRepository;
            _codeRepository = codeRepository;
            _eduFromRepository = eduFromRepository;
            _examTypeRepository = examTypeRepository;
            _subjectRepository = subjectRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task ImportToDatabaseAsync(MemoryStream fileStream, string fileName, CancellationToken ct)
        {
            // Sorry for b(ы)dlo-code
            // The flow can bearly got shortened or decomposed. It`s gonna cause even more 
            // overengineering ("spaghettiness") and for now it`s better to leave it as it is.
            
            IExcelDataReader excelDataReader;
            
            if (fileName.EndsWith(".xls"))
            {
                excelDataReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
            }
            else if (fileName.EndsWith(".xlsx"))
            {
                excelDataReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
            }
            else
            {
                throw new FormatException("The file format is not supported.");
            }

            var records = excelDataReader.AsDataSet();
            excelDataReader.Close();

            if (records != null && records.Tables.Count > 0)
            {
                var dtRecords = records.Tables[0];

                var subjectPriority = 0;

                var prevCode = "";
                var prevEduForm = "";

                for (var count = 1; count < dtRecords.Rows.Count; count++)
                {
                    var importedCode = dtRecords.Rows[count][0].ToString();
                    var eduForms = dtRecords.Rows[count][1].ToString();
                    var subjectName = dtRecords.Rows[count][2].ToString();
                    var replacedSubjectName = dtRecords.Rows[count][3].ToString();
                    var examTypes = dtRecords.Rows[count][4].ToString();
                    var minScore = Convert.ToInt32(dtRecords.Rows[count][5]);
                    
                    if (prevCode != importedCode || prevEduForm != eduForms)
                    {
                        subjectPriority = 1;
                        prevCode = importedCode;
                        prevEduForm = eduForms;
                    }
                    else if(replacedSubjectName.Length == 0)
                    {
                        subjectPriority += 1;
                    }

                    var newCode = new Code();
                    newCode.Value = importedCode;
                    newCode.MinScore = minScore;
                    
                    var existingSubject = await _subjectRepository.QueryableSelect()
                        .SingleOrDefaultAsync(x => x.Title == subjectName);
                    
                    var existingReplacedSubject = await _subjectRepository.QueryableSelect()
                        .SingleOrDefaultAsync(x => x.Title == replacedSubjectName);
                    
                    if (existingSubject == null)
                    {
                        var newSubject = new Subject();
                        newSubject.Title = subjectName;
                        newSubject.Priority = subjectPriority;

                        await _subjectRepository.ApplyAsync(newSubject, ct);
                        await _unitOfWork.SaveAsync(ct);

                        newCode.SubjectId = newSubject.Id;
                    }
                    else
                    {
                        newCode.SubjectId = existingSubject.Id;
                    }

                    if (replacedSubjectName.Length != 0)
                    {
                        if (existingReplacedSubject == null)
                        {
                            var newSubject = new Subject();
                            newSubject.Title = replacedSubjectName;
                            newSubject.Priority = subjectPriority;

                            await _subjectRepository.ApplyAsync(newSubject, ct);
                            await _unitOfWork.SaveAsync(ct);

                            newCode.ReplacedSubjectId = newSubject.Id;
                        }
                        else
                        {
                            newCode.ReplacedSubjectId = existingReplacedSubject.Id;
                        }
                    }

                    await _codeRepository.ApplyAsync(newCode, ct);
                    await _unitOfWork.SaveAsync(ct);

                    
                    var examTypesArray = examTypes?.Split(";");
                    var eduFormsArray = eduForms?.Split(";");

                    if (examTypesArray != null)
                    {
                        foreach (var type in examTypesArray)
                        {
                            var trimmedType = type.Trim(' ');
                            var existingExamType = await _examTypeRepository.QueryableSelect()
                                .SingleOrDefaultAsync(x => x.Title == trimmedType, ct);

                            if (existingExamType == null)
                            {
                                var newExamType = new ExamType();
                                newExamType.Title = trimmedType;

                                await _examTypeRepository.ApplyAsync(newExamType, ct);
                                await _unitOfWork.SaveAsync(ct);

                                var newExamTypeMapping = new CodeExamTypeMapping();
                                
                                newExamTypeMapping.CodeId = newCode.Id;
                                newExamTypeMapping.ExamTypeId = newExamType.Id;
                                
                                await _examTypeMappingRepository.ApplyAsync(newExamTypeMapping, ct);
                                await _unitOfWork.SaveAsync();
                            }
                            else
                            {
                                var newExamTypeMapping = new CodeExamTypeMapping();
                                
                                newExamTypeMapping.CodeId = newCode.Id;
                                newExamTypeMapping.ExamTypeId = existingExamType.Id;
                                
                                await _examTypeMappingRepository.ApplyAsync(newExamTypeMapping, ct);
                                await _unitOfWork.SaveAsync();
                            }
                        }
                    }
                    
                    if (eduFormsArray != null)
                    {
                        foreach (var form in eduFormsArray)
                        {
                            var trimmedForm = form.Trim(' ');

                            var existingEduFrom = await _eduFromRepository.QueryableSelect()
                                .SingleOrDefaultAsync(x => x.Title == trimmedForm, ct);

                            if (existingEduFrom == null)
                            {
                                var newEduForm = new EduForm();
                                newEduForm.Title = trimmedForm;

                                await _eduFromRepository.ApplyAsync(newEduForm, ct);
                                await _unitOfWork.SaveAsync(ct);

                                var newEduFormMapping = new CodeEduFormMapping();
                                newEduFormMapping.CodeId = newCode.Id;
                                newEduFormMapping.EduFormId = newEduForm.Id;

                                await _codeEduFormMappingRepository.ApplyAsync(newEduFormMapping, ct);
                                await _unitOfWork.SaveAsync(ct);
                            }
                            else
                            {
                                var newEduFormMapping = new CodeEduFormMapping();
                                newEduFormMapping.CodeId = newCode.Id;
                                newEduFormMapping.EduFormId = existingEduFrom.Id;

                                await _codeEduFormMappingRepository.ApplyAsync(newEduFormMapping, ct);
                                await _unitOfWork.SaveAsync(ct);
                            }
                        }
                    }
                    
                    await _codeRepository.ApplyAsync(newCode, ct);
                    await _unitOfWork.SaveAsync(ct);
                    
                }
            }
        }
    }
}