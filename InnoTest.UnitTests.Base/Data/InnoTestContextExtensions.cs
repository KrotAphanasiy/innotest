﻿using System.Threading.Tasks;
using InnoTest.Data;
using InnoTest.Foundation.Domain;

namespace InnoTest.UnitTests.Data
{
    public static class InnoTestContextExtensions
    {
        public static async Task SeedSubjects(this InnoTestContext context)
        {
            var newSubject = new Subject
            {
                Id = 96,
                Title = "математика"
            };

            await context.Subjects.AddAsync(newSubject);
            await context.SaveChangesAsync();
        }
        
        public static async Task  SeedCodes(this InnoTestContext context)
        {
            var newCode = new Code
            {
                Id = 478,
                Value = "ММ_О_ГБ",
                SubjectId = 96,
                MinScore = 50,
                ReplacedSubject = null
            };

            await context.Codes.AddAsync(newCode);
            await context.SaveChangesAsync();
        }
        
        public static async Task  SeedEduForms(this InnoTestContext context)
        {
            var newEduFrom = new EduForm
            {
                Id = 35,
                Title = "Высшее"
            };

            await context.EduForms.AddAsync(newEduFrom);
            await context.SaveChangesAsync();
        }
        
        public static async Task  SeedExamTypes(this InnoTestContext context)
        {
            var newExamType = new ExamType
            {
                Id = 18,
                Title = "Экзамен"
            };

            await context.ExamTypes.AddAsync(newExamType);
            await context.SaveChangesAsync();
        }
        
        public static async Task  SeedCodeEduFormMappings(this InnoTestContext context)
        {
            var mapping = new CodeEduFormMapping
            {
                Id = 611,
                CodeId = 478,
                EduFormId = 35
            };

            await context.CodeEduFormMappings.AddAsync(mapping);
            await context.SaveChangesAsync();
        }
        
        public static async Task  SeedCodeExamTypeMappings(this InnoTestContext context)
        {
            var mapping = new CodeExamTypeMapping
            {
                Id = 759,
                CodeId = 478,
                ExamTypeId = 18
            };

            await context.CodeExamTypeMappings.AddAsync(mapping);
            await context.SaveChangesAsync();
        }
    }
}