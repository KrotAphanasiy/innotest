﻿using InnoTest.Data.Repositories.Interfaces;
using InnoTest.Foundation.Domain;
using Microsoft.EntityFrameworkCore;

namespace InnoTest.Data.Repositories
{
    public class ExamTypeRepository: BaseRepository<ExamType>, IExamTypeRepository
    {
        public ExamTypeRepository(InnoTestContext context, QueryTrackingBehavior queryTrackingBehavior = QueryTrackingBehavior.TrackAll) : base(context, queryTrackingBehavior)
        {
        }
    }
}