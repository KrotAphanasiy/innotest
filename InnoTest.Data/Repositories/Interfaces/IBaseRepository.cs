﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Query;

namespace InnoTest.Data.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity>: IDisposable
    {
        Task ExecuteSql(FormattableString sql, CancellationToken ct);

        #region Select

        /// <summary>
        /// Returns an array of objects
        /// </summary>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <param name="includes">Expression to include related data</param>
        /// <returns></returns>
        public Task<TEntity[]> SelectAsync(CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Returns an array of objects by condition
        /// </summary>
        /// <param name="predicate">Сondition</param>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <param name="includes">Expression to include related data</param>
        /// <returns></returns>
        public Task<TEntity[]> SelectByConditionAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Returns an array of objects by condition with related data
        /// </summary>
        /// <param name="predicate">Сondition</param>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <param name="includes">Expression to include a chaining of related data</param>
        /// <returns></returns>
        public Task<TEntity[]> SelectIncludeAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>[] includes);

        #endregion


        #region  Get by Id, First, FirsOrDefault, Single, SingleOrDefault

        /// <summary>
        /// Returns an object by ID
        /// </summary>
        /// <typeparam name="TEntity">A class that implements <see cref="IEntity"/> </typeparam>
        /// <param name="id">ID</param>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <returns></returns>
        public Task<TEntity> GetByIdAsync(int id, CancellationToken cancellationToken = default);

        /// <summary>
        /// Returns an object by ID with inclusions
        /// </summary>
        /// <typeparam name="TEntity">A class that implements <see cref="IEntity"/> </typeparam>
        /// <param name="id">ID</param>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <param name="includes">Expression to include related data</param>
        /// <returns></returns>
        Task<TEntity> GetByIdAsync(int id, CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Returns the first found object or null
        /// </summary>
        /// <param name="predicate">Сondition</param>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <param name="includes">Expression to include related data</param>
        /// <returns></returns>
        public Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Returns the first found object or null
        /// </summary>
        /// <param name="predicate">Сondition</param>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <param name="includes">Expression to include related data</param>
        /// <returns></returns>
        public Task<TEntity> FirstOrDefaultIncludeAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>[] includes);

        /// <summary>
        /// Returns a record if it is the only one in the database
        /// </summary>
        /// <param name="predicate">Сondition</param>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <param name="includes">Expression to include related data</param>
        public Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

        IQueryable<TEntity> TrackingQueryableSelect(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);
        #endregion


        #region Queryable

        /// <summary>
        /// Receives data from a database with inclusions
        /// </summary>
        public IQueryable<TEntity> QueryableSelect(params Expression<Func<TEntity, object>>[] includes);
        
        /// <summary>
        /// Receives data from a database with inclusions
        /// </summary>
        public IQueryable<TEntity> TrackingQueryableSelect(params Expression<Func<TEntity, object>>[] includes);


        /// <summary>
        /// Receives data from the database by condition with inclusions
        /// </summary>
        public IQueryable<TEntity> QueryableSelect(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Receives data from the database by condition with inclusions (+related data)
        /// </summary>
        public IQueryable<TEntity> QueryableInclude(Expression<Func<TEntity, bool>> predicate, params Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>[] includes);

        #endregion 


        #region Aggregate function

        /// <summary>
        /// Returns the number of elements that satisfy a condition
        /// </summary>
        /// <param name="predicate">Сondition</param>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <returns></returns>
        public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate = null, CancellationToken cancellationToken = default);

        /// <summary>
        /// Checks for the presence of elements by a given condition
        /// </summary>
        /// <param name="predicate">Сondition</param>
        /// <param name="cancellationToken">Request cancellation token</param>
        /// <returns></returns>
        public Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate = null, CancellationToken cancellationToken = default);

        #endregion       

        /// <summary>
        /// Adds or updates an entity in a database context without saving changes
        /// </summary>
        /// <param name="entity">A class that implements<see cref="IEntity"/></param>
        /// <param name="ct">Request cancellation token</param>
        Task<TEntity> ApplyAsync(TEntity entity, CancellationToken ct = default);
        
        /// <param name="entity">A class that implements<see cref="IEntity"/></param>
        void Add(TEntity entity);

        /// <summary>
        /// Removes an entity from the context without subsequently saving changes
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="ct">Request cancellation token</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(TEntity entity, CancellationToken ct = default);

        /// <summary>
        /// Removes an entity from the context without subsequently saving changes
        /// </summary>
        /// <param name="entityId">Entity id</param>
        /// <param name="ct">Request cancellation token</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(int entityId, CancellationToken ct = default);

        /// <summary>
        /// Removes a list of entities from the context without subsequently saving changes
        /// </summary>
        /// <param name="entities">Entity List</param>
        /// <param name="ct">Request cancellation token</param>
        /// <returns></returns>
        Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken ct = default);

        void Remove(TEntity entity);

        void RemoveRange(IEnumerable<TEntity> entities);
    }
}