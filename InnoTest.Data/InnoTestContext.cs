﻿using System;
using System.Threading;
using System.Threading.Tasks;
using InnoTest.Foundation.Domain;
using InnoTest.Foundation.Domain.Base;
using Microsoft.EntityFrameworkCore;

namespace InnoTest.Data
{
    public class InnoTestContext: DbContext
    {
        public InnoTestContext(DbContextOptions<InnoTestContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Code>()
                .HasOne(x => x.Subject);
            modelBuilder.Entity<Code>()
                .HasOne(x => x.ReplacedSubject);


            modelBuilder.Entity<CodeEduFormMapping>()
                .HasOne(x => x.EduForm);
            modelBuilder.Entity<CodeEduFormMapping>()
                .HasOne(x => x.Code);

            modelBuilder.Entity<CodeExamTypeMapping>()
                .HasOne(x => x.ExamType);
            modelBuilder.Entity<CodeExamTypeMapping>()
                .HasOne(x => x.Code);
        }
        
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }
        
        private void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity is BaseEntity baseEntity)
                {
                    var now = DateTime.UtcNow;

                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            baseEntity.UpdatedDate = now;
                            break;

                        case EntityState.Added:
                            baseEntity.CreatedDate = now;
                            baseEntity.UpdatedDate = now;
                            break;
                    }
                }
            }
        }
        
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<Code> Codes { get; set; }
        public virtual DbSet<EduForm> EduForms { get; set; }
        public virtual DbSet<ExamType> ExamTypes { get; set; }
        public virtual DbSet<CodeEduFormMapping> CodeEduFormMappings { get; set; }
        public virtual DbSet<CodeExamTypeMapping> CodeExamTypeMappings { get; set; }
    }
}