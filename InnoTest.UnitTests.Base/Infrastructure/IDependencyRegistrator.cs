﻿using System;

namespace InnoTest.UnitTests.Infrastructure
{
    public interface IDependencyRegistrator
    {
        IServiceProvider GetServiceProvider();
    }
}
