﻿using InnoTest.Core.Services;
using InnoTest.Core.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace InnoTest.Core.Configuration
{
    public static class CoreLayerDependencyConfigurator
    {
        public static void ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<IExcelImportService, ExcelImportService>();
        }
        
    }
}