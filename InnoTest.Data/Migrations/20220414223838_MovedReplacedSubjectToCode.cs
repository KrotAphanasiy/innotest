﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InnoTest.Data.Migrations
{
    public partial class MovedReplacedSubjectToCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subjects_Subjects_ReplacedSubjectId",
                table: "Subjects");

            migrationBuilder.DropIndex(
                name: "IX_Subjects_ReplacedSubjectId",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "ReplacedSubjectId",
                table: "Subjects");

            migrationBuilder.AddColumn<int>(
                name: "ReplacedSubjectId",
                table: "Codes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Codes_ReplacedSubjectId",
                table: "Codes",
                column: "ReplacedSubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Codes_Subjects_ReplacedSubjectId",
                table: "Codes",
                column: "ReplacedSubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Codes_Subjects_ReplacedSubjectId",
                table: "Codes");

            migrationBuilder.DropIndex(
                name: "IX_Codes_ReplacedSubjectId",
                table: "Codes");

            migrationBuilder.DropColumn(
                name: "ReplacedSubjectId",
                table: "Codes");

            migrationBuilder.AddColumn<int>(
                name: "ReplacedSubjectId",
                table: "Subjects",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subjects_ReplacedSubjectId",
                table: "Subjects",
                column: "ReplacedSubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subjects_Subjects_ReplacedSubjectId",
                table: "Subjects",
                column: "ReplacedSubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
