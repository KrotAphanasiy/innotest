﻿using InnoTest.Foundation.Domain.Base;

namespace InnoTest.Foundation.Domain
{
    public class CodeEduFormMapping: BaseEntity
    {
        public int CodeId { get; set; }
        public int EduFormId { get; set; }
        
        public virtual Code Code { get; set; }
        public virtual EduForm EduForm { get; set; }
    }
}