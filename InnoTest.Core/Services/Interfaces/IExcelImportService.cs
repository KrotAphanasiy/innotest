﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace InnoTest.Core.Services.Interfaces
{
    public interface IExcelImportService
    {
        public Task ImportToDatabaseAsync(MemoryStream fileStream, string fileName, CancellationToken ct);
    }
}