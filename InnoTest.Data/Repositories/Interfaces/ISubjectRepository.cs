﻿using InnoTest.Foundation.Domain;

namespace InnoTest.Data.Repositories.Interfaces
{
    public interface ISubjectRepository: IBaseRepository<Subject>
    {
        
    }
}