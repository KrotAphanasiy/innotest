﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InnoTest.Data.Migrations
{
    public partial class MovedMinScoreToCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MinScore",
                table: "Subjects");

            migrationBuilder.AddColumn<int>(
                name: "MinScore",
                table: "Codes",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MinScore",
                table: "Codes");

            migrationBuilder.AddColumn<int>(
                name: "MinScore",
                table: "Subjects",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
