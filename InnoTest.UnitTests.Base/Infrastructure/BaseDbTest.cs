﻿using System.Threading.Tasks;
using InnoTest.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace InnoTest.UnitTests.Infrastructure
{
    public abstract class BaseDbTest<TDependencyRegistrator, TDbContext> : BaseTest<TDependencyRegistrator>
        where TDependencyRegistrator : IDependencyRegistrator, new()
        where TDbContext : InnoTestContext
    {
        protected TDbContext DbContext { get; }

        protected BaseDbTest()
        {
            DbContext = ServiceProvider.GetRequiredService<TDbContext>();
        }

        #region Interface implementations

        public override Task InitializeAsync()
        {
            return Task.CompletedTask;
        }
        
        public override Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        #endregion
        
        protected virtual Task SeedAsync() => Task.CompletedTask;

        protected void IgnoreConstraints()
        {
            DbContext.Database.ExecuteSqlRaw("PRAGMA foreign_keys=OFF;");
            DbContext.Database.ExecuteSqlRaw("PRAGMA ignore_check_constraints=OFF;");
        }
    }
}
