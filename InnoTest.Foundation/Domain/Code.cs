﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InnoTest.Foundation.Domain.Base;

namespace InnoTest.Foundation.Domain
{
    public class Code: BaseEntity
    {
        [MaxLength(16)]
        public string Value { get; set; }

        public int MinScore { get; set; }
        
        public int SubjectId { get; set; }
        
        public int? ReplacedSubjectId { get; set; }

        public virtual Subject Subject { get; set; }

        public virtual Subject ReplacedSubject { get; set; }
    }
}