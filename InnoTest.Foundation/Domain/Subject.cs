﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using InnoTest.Foundation.Domain.Base;

namespace InnoTest.Foundation.Domain
{
    public class Subject: BaseEntity
    {
        [MaxLength(256)]
        public string Title { get; set; }

        public int Priority { get; set; }
    }
}