﻿using System.Threading;
using System.Threading.Tasks;

namespace InnoTest.Data.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        Task SaveAsync(CancellationToken cancellationToken = default);

        void ClearTracker();
    }
}