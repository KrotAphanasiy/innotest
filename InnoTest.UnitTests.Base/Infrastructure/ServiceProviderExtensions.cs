﻿using System;
using InnoTest.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace InnoTest.UnitTests.Infrastructure
{
    public static class ServiceProviderExtensions
    {
        public static void MigrateDatabase(this IServiceProvider serviceProvider)
        {
            var dbContext = serviceProvider.GetRequiredService<InnoTestContext>();
            dbContext.Database.Migrate();
        }
    }
}