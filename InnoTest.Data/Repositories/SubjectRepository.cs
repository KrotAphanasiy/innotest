﻿using InnoTest.Data.Repositories.Interfaces;
using InnoTest.Foundation.Domain;
using Microsoft.EntityFrameworkCore;

namespace InnoTest.Data.Repositories
{
    public class SubjectRepository: BaseRepository<Subject>, ISubjectRepository
    {
        public SubjectRepository(InnoTestContext context, QueryTrackingBehavior queryTrackingBehavior = QueryTrackingBehavior.TrackAll) : base(context, queryTrackingBehavior)
        {
        }
    }
}