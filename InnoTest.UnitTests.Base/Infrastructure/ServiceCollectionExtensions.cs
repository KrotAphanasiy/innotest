﻿using InnoTest.Core.Services;
using InnoTest.Core.Services.Interfaces;
using InnoTest.Data;
using InnoTest.Data.Repositories;
using InnoTest.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace InnoTest.UnitTests.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDbContext<TDbContext>(this IServiceCollection services)
            where TDbContext : DbContext
        {
            services.AddDbContext<TDbContext>(options =>
            {
                options.EnableSensitiveDataLogging();
                options.UseSqlite("Data Source=database.db;",
                    x => x.MigrationsAssembly("InnoTest.UnitTest.Migrations"));
            });
            services.AddScoped<ICodeEduFormMappingRepository, CodeEduFormMappingRepository>();
            services.AddScoped<ICodeExamTypeMappingRepository, CodeExamTypeMappingRepository>();
            services.AddScoped<ICodeRepository, CodeRepository>();
            services.AddScoped<IEduFromRepository, EduFormRepository>();
            services.AddScoped<IExamTypeRepository, ExamTypeRepository>();
            services.AddScoped<ISubjectRepository, SubjectRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork<InnoTestContext>>();
            
            services.AddScoped<IExcelImportService, ExcelImportService>();
            
            
            return services;
        }
        
        
    }
}
