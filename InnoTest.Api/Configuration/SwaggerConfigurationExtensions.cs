﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace InnoTest.Configuration
{
    public static class SwaggerConfigurationExtensions
    {
        public static void AddSwaggerDocumentation(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "InnoTest API",
                    Description = "InnoTest"
                });

                c.DescribeAllParametersInCamelCase();
               
            });
        }
        
        public static void UseSwaggerDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger(s =>
            {
                s.RouteTemplate = "api/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/api/v1/swagger.json", "InnoTest API V1");
                c.RoutePrefix = "swagger";
            });
        }
    }
}