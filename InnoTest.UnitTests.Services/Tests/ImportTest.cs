﻿using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using InnoTest.Core.Services.Interfaces;
using InnoTest.Data;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace InnoTest.UnitTests.Services.Tests
{
    public class ImportTest: BaseDbTest
    {
        private readonly IExcelImportService _excelImportService;
        private readonly InnoTestContext _dbContext;
        
        public ImportTest()
        {
            _dbContext = ServiceProvider.GetRequiredService<InnoTestContext>();
            _excelImportService = ServiceProvider.GetRequiredService<IExcelImportService>();
        }

        [Fact]
        public async Task TestImport()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            
            var ms = new MemoryStream();
            using (var fs = new FileStream($"../../../Data/items.xlsx", FileMode.Open, FileAccess.Read))
            {
                await fs.CopyToAsync(ms);
                await _excelImportService.ImportToDatabaseAsync(ms, fs.Name, CancellationToken.None);
            }
            
            _dbContext.Database.EnsureDeleted();
        }
    }
}