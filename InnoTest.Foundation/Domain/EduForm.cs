﻿using System.ComponentModel.DataAnnotations;
using InnoTest.Foundation.Domain.Base;

namespace InnoTest.Foundation.Domain
{
    public class EduForm: BaseEntity
    {
        [MaxLength(256)]
        public string Title { get; set; }
    }
}