﻿using InnoTest.Data.Repositories.Interfaces;
using InnoTest.Foundation.Domain;
using Microsoft.EntityFrameworkCore;

namespace InnoTest.Data.Repositories
{
    public class EduFormRepository: BaseRepository<EduForm>, IEduFromRepository
    {
        public EduFormRepository(InnoTestContext context, QueryTrackingBehavior queryTrackingBehavior = QueryTrackingBehavior.TrackAll) : base(context, queryTrackingBehavior)
        {
        }
    }
}