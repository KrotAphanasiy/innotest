﻿using System;
using InnoTest.Data;
using InnoTest.UnitTests.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace InnoTest.UnitTests
{
    public class Startup : IDependencyRegistrator
    {
        public IServiceProvider GetServiceProvider()
        {
            var services = new ServiceCollection();
            services.AddDbContext<InnoTestContext>();
            
            return services.BuildServiceProvider();
        }
    }
}
